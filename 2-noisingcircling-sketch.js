let pointInterval = 60; // Number of frames between point appearances
let frameCount = 0; // Counter for the current frame
let prevX, prevY; // Coordinates of the previous point


function setup() {

  createCanvas(windowWidth, windowHeight);
  frameRate(1);
  background(0, 0, 255);

}


function draw() {
  let fps = frameRate();
  let x = random(width);
  let y = random(height);
  let ptweight = random(1, 300);
  let ptopacity = random(1, 30);
  let static_weight = floor(random(1, 20));

  //point
  stroke(255, ptopacity);
  strokeWeight(ptweight);
  point(x, y);


  //line
  if (prevX !== undefined && prevY !== undefined) {
    stroke(0, 20);
    strokeWeight(1);
    line(prevX, prevY, x, y);
  }

  prevX = x;
  prevY = y;

  frameCount++; // Increment the frame count


  //mouse press
  if (mouseIsPressed === true) {
    stroke(255);
    strokeWeight(static_weight);
    point(mouseX, mouseY); 
    
  } else {
    noStroke();
    point(mouseX, mouseY); 
  }




  //cursor
  cursor(HAND);

  stroke(255);
  strokeWeight(2);
  point(pmouseX, pmouseY); 

  stroke(255, 204, 0);
  strokeWeight(2);
  point(mouseX, mouseY); 



}